<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function login (LoginRequest $request)
    {

        if ($request->post('email') == 'robbe@test.be' && $request->post('password') == '123') {
            return redirect(route('member.dashboard'));
        }

        return redirect()
            ->back()
            ->withInput($request->only('email'))
            ->withErrors([
                'auth' => 'Credentials not found'
            ])
        ;

    }
}
