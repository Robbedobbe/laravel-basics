@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="post" action="{{ route('login.post') }}">
    @csrf
    <div class="form-group">
        <input type="text" class="form-control" name="email" placeholder="e-mail" value="{{ old('email') }}">
    </div>

    <div class="form-group">
        <input type="password" class="form-control" name="password" placeholder="wachtwoord">
    </div>

    <input type="submit" class="btn btn-primary btn-block" value="Log In">
</form>