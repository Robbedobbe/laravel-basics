@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-8 offset-2 mt-4">
            <div class="card">
                <div class="card-body">
                    <h1>Login:</h1>
                    @include('forms.loginform')

                </div>
            </div>
        </div>
    </div>
@endsection